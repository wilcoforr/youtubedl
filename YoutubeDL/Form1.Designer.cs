﻿namespace YoutubeDL
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_YoutubeUrl = new System.Windows.Forms.TextBox();
            this.btn_CheckForUpdates = new System.Windows.Forms.Button();
            this.tb_log = new System.Windows.Forms.TextBox();
            this.btn_DownloadAsM4AAudio = new System.Windows.Forms.Button();
            this.btn_DownloadVideo720p = new System.Windows.Forms.Button();
            this.btn_OpenFolder = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lbl_EnterYoutubeUrl = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.youtubedlexePageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.youtubeDLPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_DownloadAsWebmAudio = new System.Windows.Forms.Button();
            this.btn_DownloadBest = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tb_YoutubeUrl
            // 
            this.tb_YoutubeUrl.Location = new System.Drawing.Point(11, 55);
            this.tb_YoutubeUrl.MaxLength = 120;
            this.tb_YoutubeUrl.Name = "tb_YoutubeUrl";
            this.tb_YoutubeUrl.Size = new System.Drawing.Size(500, 20);
            this.tb_YoutubeUrl.TabIndex = 0;
            // 
            // btn_CheckForUpdates
            // 
            this.btn_CheckForUpdates.Location = new System.Drawing.Point(11, 197);
            this.btn_CheckForUpdates.Name = "btn_CheckForUpdates";
            this.btn_CheckForUpdates.Size = new System.Drawing.Size(500, 23);
            this.btn_CheckForUpdates.TabIndex = 1;
            this.btn_CheckForUpdates.Text = "Check for youtube-dl.exe Updates";
            this.btn_CheckForUpdates.UseVisualStyleBackColor = true;
            this.btn_CheckForUpdates.Click += new System.EventHandler(this.Btn_CheckForUpdates_Click);
            // 
            // tb_log
            // 
            this.tb_log.Location = new System.Drawing.Point(11, 255);
            this.tb_log.Multiline = true;
            this.tb_log.Name = "tb_log";
            this.tb_log.ReadOnly = true;
            this.tb_log.Size = new System.Drawing.Size(500, 183);
            this.tb_log.TabIndex = 2;
            // 
            // btn_DownloadAsM4AAudio
            // 
            this.btn_DownloadAsM4AAudio.Location = new System.Drawing.Point(11, 110);
            this.btn_DownloadAsM4AAudio.Name = "btn_DownloadAsM4AAudio";
            this.btn_DownloadAsM4AAudio.Size = new System.Drawing.Size(500, 23);
            this.btn_DownloadAsM4AAudio.TabIndex = 4;
            this.btn_DownloadAsM4AAudio.Text = "Download as M4A audio";
            this.btn_DownloadAsM4AAudio.UseVisualStyleBackColor = true;
            this.btn_DownloadAsM4AAudio.Click += new System.EventHandler(this.Btn_QuickDownloadAudio_Click);
            // 
            // btn_DownloadVideo720p
            // 
            this.btn_DownloadVideo720p.Location = new System.Drawing.Point(11, 168);
            this.btn_DownloadVideo720p.Name = "btn_DownloadVideo720p";
            this.btn_DownloadVideo720p.Size = new System.Drawing.Size(500, 23);
            this.btn_DownloadVideo720p.TabIndex = 5;
            this.btn_DownloadVideo720p.Text = "Download as 720p MP4 video and audio";
            this.btn_DownloadVideo720p.UseVisualStyleBackColor = true;
            this.btn_DownloadVideo720p.Click += new System.EventHandler(this.Btn_QuickDownloadVideo720p_Click);
            // 
            // btn_OpenFolder
            // 
            this.btn_OpenFolder.Location = new System.Drawing.Point(11, 226);
            this.btn_OpenFolder.Name = "btn_OpenFolder";
            this.btn_OpenFolder.Size = new System.Drawing.Size(500, 23);
            this.btn_OpenFolder.TabIndex = 7;
            this.btn_OpenFolder.Text = "Open Downloads Folder";
            this.btn_OpenFolder.UseVisualStyleBackColor = true;
            this.btn_OpenFolder.Click += new System.EventHandler(this.Btn_OpenFolder_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(11, 444);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(500, 25);
            this.progressBar1.TabIndex = 8;
            // 
            // lbl_EnterYoutubeUrl
            // 
            this.lbl_EnterYoutubeUrl.AutoSize = true;
            this.lbl_EnterYoutubeUrl.Location = new System.Drawing.Point(12, 36);
            this.lbl_EnterYoutubeUrl.Name = "lbl_EnterYoutubeUrl";
            this.lbl_EnterYoutubeUrl.Size = new System.Drawing.Size(157, 13);
            this.lbl_EnterYoutubeUrl.TabIndex = 9;
            this.lbl_EnterYoutubeUrl.Text = "Enter the YouTube URL Below:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(520, 24);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.youtubedlexePageToolStripMenuItem,
            this.youtubeDLPageToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.fileToolStripMenuItem.Text = "Other";
            // 
            // youtubedlexePageToolStripMenuItem
            // 
            this.youtubedlexePageToolStripMenuItem.Name = "youtubedlexePageToolStripMenuItem";
            this.youtubedlexePageToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.youtubedlexePageToolStripMenuItem.Text = "youtube-dl.exe page";
            this.youtubedlexePageToolStripMenuItem.Click += new System.EventHandler(this.YoutubedlexePageToolStripMenuItem_Click);
            // 
            // youtubeDLPageToolStripMenuItem
            // 
            this.youtubeDLPageToolStripMenuItem.Name = "youtubeDLPageToolStripMenuItem";
            this.youtubeDLPageToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.youtubeDLPageToolStripMenuItem.Text = "YoutubeDL page";
            this.youtubeDLPageToolStripMenuItem.Click += new System.EventHandler(this.YoutubeDLPageToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // btn_DownloadAsWebmAudio
            // 
            this.btn_DownloadAsWebmAudio.Location = new System.Drawing.Point(11, 139);
            this.btn_DownloadAsWebmAudio.Name = "btn_DownloadAsWebmAudio";
            this.btn_DownloadAsWebmAudio.Size = new System.Drawing.Size(500, 23);
            this.btn_DownloadAsWebmAudio.TabIndex = 11;
            this.btn_DownloadAsWebmAudio.Text = "Download as Webm audio";
            this.btn_DownloadAsWebmAudio.UseVisualStyleBackColor = true;
            this.btn_DownloadAsWebmAudio.Click += new System.EventHandler(this.Btn_DownloadAsWebmAudio_Click);
            // 
            // btn_DownloadBest
            // 
            this.btn_DownloadBest.Location = new System.Drawing.Point(11, 81);
            this.btn_DownloadBest.Name = "btn_DownloadBest";
            this.btn_DownloadBest.Size = new System.Drawing.Size(500, 23);
            this.btn_DownloadBest.TabIndex = 12;
            this.btn_DownloadBest.Text = "Download as Best Quality";
            this.btn_DownloadBest.UseVisualStyleBackColor = true;
            this.btn_DownloadBest.Click += new System.EventHandler(this.Btn_DownloadBest_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 478);
            this.Controls.Add(this.btn_DownloadBest);
            this.Controls.Add(this.btn_DownloadAsWebmAudio);
            this.Controls.Add(this.lbl_EnterYoutubeUrl);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.btn_OpenFolder);
            this.Controls.Add(this.btn_DownloadVideo720p);
            this.Controls.Add(this.btn_DownloadAsM4AAudio);
            this.Controls.Add(this.tb_log);
            this.Controls.Add(this.btn_CheckForUpdates);
            this.Controls.Add(this.tb_YoutubeUrl);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Youtube DL";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_YoutubeUrl;
        private System.Windows.Forms.Button btn_CheckForUpdates;
        private System.Windows.Forms.TextBox tb_log;
        private System.Windows.Forms.Button btn_DownloadAsM4AAudio;
        private System.Windows.Forms.Button btn_DownloadVideo720p;
        private System.Windows.Forms.Button btn_OpenFolder;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label lbl_EnterYoutubeUrl;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem youtubedlexePageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem youtubeDLPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button btn_DownloadAsWebmAudio;
        private System.Windows.Forms.Button btn_DownloadBest;
    }
}

