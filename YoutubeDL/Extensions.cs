﻿using System;
using System.Windows.Forms;

namespace YoutubeDL
{
    public static class Extensions
    {
        public static string DateTimeNowString => DateTime.Now.ToShortTimeString() + ": ";

        public static void AppendTextWithNewline(this TextBox textBox, string text)
        {
            textBox.AppendText(DateTimeNowString + text + Environment.NewLine);
        }

        public static void AppendSeparator(this TextBox textBox)
        {
            textBox.AppendTextWithNewline(new string('-', 60));
        }
    }

}
