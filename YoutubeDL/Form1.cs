using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using RunProcessAsTask; //https://github.com/jamesmanning/RunProcessAsTask

namespace YoutubeDL
{
    public partial class Form1 : Form
    {
        public const string YOUTUBE_DL_EXE = "youtube-dl.exe";
        public const string DOWNLOADS_FOLDER = "Downloads";

        public Form1()
        {
            InitializeComponent();

            tb_log.AppendTextWithNewline("Youtube DL started.");
            tb_log.AppendSeparator();

            if (!File.Exists(YOUTUBE_DL_EXE))
            {
                string errorMessage = "youtube-dl.exe not found." 
                    + Environment.NewLine
                    + "youtube-dl.exe must be in the same folder as this program (YoutubeDL)."
                    + Environment.NewLine
                    + "You can go to the youtube-dl.exe site to download by going to the 'Other' menu item, and go to the 'youtube-dl.exe page'";


                MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                tb_log.AppendTextWithNewline(errorMessage);

                ToggleUI();
            }

            if (!Directory.Exists(DOWNLOADS_FOLDER))
            {
                Directory.CreateDirectory(DOWNLOADS_FOLDER);
            }
        }

        private static IProgress<int> progress;

        /// <summary>
        /// Download the video
        /// </summary>
        /// <param name="formatCode"></param>
        private async void DownloadYoutubeVideo(DownloadFormat formatCode)
        {
            try
            {
                tb_log.AppendTextWithNewline("Downloading page data...");
                ToggleUI();

                progress = new Progress<int>(value => { progressBar1.Value = value; });

                progress.Report(10);

                await Task.Delay(2000);
                progress.Report(30);
                tb_log.AppendTextWithNewline("Downloading video...");

                string arguments = "";

                if (formatCode == DownloadFormat.BEST)
                {
                    arguments = tb_YoutubeUrl.Text + " -o " + DOWNLOADS_FOLDER + "/%(title)s-%(id)s.%(ext)s";
                }
                else
                {
                   arguments = tb_YoutubeUrl.Text + " -f " + (int)formatCode + " -o " + DOWNLOADS_FOLDER + "/%(title)s-%(id)s.%(ext)s";
                }

                StartYoutubeDlDownload(arguments);
            }
            catch (Exception ex)
            {
                tb_log.AppendSeparator();
                tb_log.AppendTextWithNewline(ex.ToString());
                tb_log.AppendSeparator();
            }
        }


        /// <summary>
        /// Start the youtube-dl.exe process for downloading a video
        /// </summary>
        /// <param name="arguments"></param>
        private async void StartYoutubeDlDownload(string arguments)
        {
            var processInfo = new ProcessStartInfo
            {
                FileName = YOUTUBE_DL_EXE,
                Arguments = arguments,
                CreateNoWindow = true
            };

            var downloadResult = await ProcessEx.RunAsync(processInfo);

            if (downloadResult.StandardOutput.Any(l => l.Contains("has already been downloaded")))
            {
                tb_log.AppendTextWithNewline("That video has already been downloaded.");
            }
            else if (downloadResult.ExitCode == 0)
            {
                string downloadMessage = "Downloaded video: " + tb_YoutubeUrl.Text + Environment.NewLine
                    + "\tin " + downloadResult.RunTime.Minutes + " minutes and " + downloadResult.RunTime.Seconds + " seconds";

                tb_log.AppendTextWithNewline(downloadMessage);

                ToggleUI();
                progress.Report(100);
            }
        }

        /// <summary>
        /// To prevent multiple instances of the process of youtube-dl to be launched, lock down the UI
        /// </summary>
        private void ToggleUI()
        {
            btn_DownloadBest.Enabled = !btn_DownloadBest.Enabled;
            btn_DownloadAsM4AAudio.Enabled = !btn_DownloadAsM4AAudio.Enabled;
            btn_DownloadAsWebmAudio.Enabled = !btn_DownloadAsWebmAudio.Enabled;
            btn_DownloadVideo720p.Enabled = !btn_DownloadVideo720p.Enabled;
            btn_CheckForUpdates.Enabled = !btn_CheckForUpdates.Enabled;
        }

        private void Btn_QuickDownloadAudio_Click(object sender, EventArgs e)
        {
            DownloadYoutubeVideo(DownloadFormat.AUDIO_M4A);
        }

        private void Btn_DownloadAsWebmAudio_Click(object sender, EventArgs e)
        {
            DownloadYoutubeVideo(DownloadFormat.AUDIO_WEBM);
        }

        private void Btn_QuickDownloadVideo720p_Click(object sender, EventArgs e)
        {
            DownloadYoutubeVideo(DownloadFormat.VIDEO_720P);
        }

        private void Btn_DownloadBest_Click(object sender, EventArgs e)
        {
            DownloadYoutubeVideo(DownloadFormat.BEST);
        }


        private void Btn_OpenFolder_Click(object sender, EventArgs e)
        {
            Process.Start(Environment.CurrentDirectory + "\\" + DOWNLOADS_FOLDER);
        }

        /// <summary>
        /// Check for an update. I havent had to do this yet so Im not sure if it will work
        /// Might need to start the program in Admin mode.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Btn_CheckForUpdates_Click(object sender, EventArgs e)
        {
            try
            {
                btn_CheckForUpdates.Text = "Checking...";

                var processInfo = new ProcessStartInfo { FileName = YOUTUBE_DL_EXE, Arguments = "-U", CreateNoWindow = true };

                var updateResult = await ProcessEx.RunAsync(processInfo);

                foreach (string line in updateResult.StandardOutput)
                {
                    tb_log.AppendTextWithNewline(line);
                }

            }
            catch (Exception ex)
            {
                tb_log.AppendSeparator();
                tb_log.AppendTextWithNewline("Error updating youtube-dl.exe: " + ex);
                tb_log.AppendSeparator();
            }
            finally
            {
                btn_CheckForUpdates.Text = "Check for youtube-dl.exe Updates";
            }
        }

        private void YoutubedlexePageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("https://ytdl-org.github.io/youtube-dl/index.html");
        }

        private void YoutubeDLPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("https://gitlab.com/wilcoforr/youtubedl");
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

    }
}
