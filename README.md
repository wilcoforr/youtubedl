# YoutubeDL

A GUI that wraps the command line [youtube-dl](https://ytdl-org.github.io/youtube-dl/index.html) program.

Input a video URL and download it as video or audio.

Featuring an asynchronous UI (so that the UI does not freeze when downloading a video)

